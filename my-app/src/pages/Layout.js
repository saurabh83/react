import { Outlet, Link } from "react-router-dom";
import './Layout.css'
const Layout = () => {
  return (
    <>
    <div class="header">
        <a href="#default" class="logo">Augments System Pvt. Ltd.</a>
        <div class="header-right">
            <Link to="/">Home</Link>
            <Link to="/contact">Contact</Link>
            <Link to="/blogs">Blogs</Link>
        </div>
    </div>
  
      <Outlet />
    </>
  )
};

export default Layout;